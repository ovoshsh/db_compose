# Docker MySQL replication
A simple docker setup to use master-slave topology
Master to Slave topology perform 'reads' and 'writes' on Master instance and can
only perform 'reads' in the Slave instance,

## Configuration
For slave by default it uses port 3307 and for master 3306, you can change it in
.env file.
For importing sql dumps just put ${file}.sql to folder build_env/sqldump/

## Run and build

cd ./db_compose
docker-compose up -d
