#!/bin/bash
BASE_PATH=$(dirname $0)

echo "Waiting for mysql to get up"
# Give 60 seconds for master and slave to come up
sleep 60

echo "Create MySQL Servers (master / slave repl)"
echo "-----------------"
echo "* Stopping slave"

mysql --host mysqlslave --port=$MYSQL_PORT -uroot -p$MYSQL_SLAVE_PASSWORD -AN -e 'STOP SLAVE;';
mysql --host mysqlmaster -uroot -p$MYSQL_MASTER_PASSWORD -AN -e 'RESET SLAVE ALL;';

echo "-----------------"
echo "* Create replication user"

mysql --host mysqlmaster -uroot -p$MYSQL_MASTER_PASSWORD -AN -e "CREATE USER '$MYSQL_REPLICATION_USER'@'%';"
mysql --host mysqlmaster -uroot -p$MYSQL_MASTER_PASSWORD -AN -e "GRANT REPLICATION SLAVE ON *.* TO '$MYSQL_REPLICATION_USER'@'%' IDENTIFIED BY '$MYSQL_REPLICATION_PASSWORD';"
mysql --host mysqlmaster -uroot -p$MYSQL_MASTER_PASSWORD -AN -e "flush privileges;"


echo "* Get master MySQL config"
echo "-----------------"

MASTER_Position=$(eval "mysql --host mysqlmaster -uroot -p$MYSQL_MASTER_PASSWORD -e 'show master status \G' | grep Position | sed -n -e 's/^.*: //p'")
MASTER_File=$(eval "mysql --host mysqlmaster -uroot -p$MYSQL_MASTER_PASSWORD -e 'show master status \G'     | grep File     | sed -n -e 's/^.*: //p'")

echo "set SLAVE to upstream MASTER"
echo "-----------------"

mysql --host mysqlslave --port=$MYSQL_PORT -uroot -p$MYSQL_SLAVE_PASSWORD -AN -e "CHANGE MASTER TO master_host='mysqlmaster', master_port=3306, master_user='$MYSQL_REPLICATION_USER', master_password='$MYSQL_REPLICATION_PASSWORD', master_log_file='$MASTER_File', master_log_pos=$MASTER_Position;"

echo "* Start sync: MASTER to SLAVE"
echo "-----------------"
mysql --host mysqlslave --port=$MYSQL_PORT -uroot -p$MYSQL_SLAVE_PASSWORD -AN -e "start slave;"
mysql --host mysqlslave --port=$MYSQL_PORT -uroot -p$MYSQL_SLAVE_PASSWORD -AN -e "show slave status \G;"

echo "Increase the max_connections to 2000"
echo "-----------------"

mysql --host mysqlmaster -uroot -p$MYSQL_MASTER_PASSWORD -AN -e 'set GLOBAL max_connections=2000';
mysql --host mysqlslave --port=$MYSQL_PORT -uroot -p$MYSQL_SLAVE_PASSWORD -AN -e 'set GLOBAL max_connections=2000';

mysql --host mysqlslave --port=$MYSQL_PORT -uroot -p$MYSQL_SLAVE_PASSWORD -e "show slave status \G"

echo "MySQL servers created!"
echo "--------------------"

echo "Import mysql dumps from the folder sqldump"
echo "-------------------"

if [ -z "$(ls -A /tmp/sqldump)" ]; then
    echo "No sql dumps in folder"
else
    for dump in `ls /tmp/sqldump/*.sql`; do mysql --host mysqlmaster -uroot -p$MYSQL_MASTER_PASSWORD < ${dump}; done
fi

echo "DONE!!!"
echo "-------------------"
